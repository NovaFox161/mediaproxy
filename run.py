# mediaproxy: mediaproxy component of litecord
# Copyright 2018-2019, Luna Mendes and the mediaproxy contributors
# SPDX-License-Identifier: AGPL-3.0-only

import sys
from configparser import ConfigParser

import logbook

from aiohttp import ClientSession
from logbook import StreamHandler, Logger
from logbook.compat import redirect_logging
from quart import Quart

from mediaproxy.bp import proxy, insertr, url_cache
from mediaproxy.errors import InvalidResponse, MimeError

app = Quart(__name__)

handler = StreamHandler(sys.stdout, level=logbook.INFO)
handler.push_application()

log = Logger('mediaproxy')
redirect_logging()


def load_blueprints(app_):
    """Load blueprints"""
    blueprints = [
        proxy, insertr, url_cache
    ]

    for bp in blueprints:
        app_.register_blueprint(bp)


def load_config(app_):
    """Load config file"""
    cfg = ConfigParser()
    cfg.read('./config.ini')

    app_.cfg = cfg
    app_.testing = False

    if cfg['mediaproxy']['test']:
        handler.level = logbook.DEBUG


load_blueprints(app)
load_config(app)

@app.before_serving
async def app_before_serving():
    """Before serving handler"""
    app.session = ClientSession()

@app.after_serving
async def app_after_serving():
    """After serving handler"""
    await app.session.close()


@app.errorhandler(InvalidResponse)
async def invalid_resp_err(_err):
    """Handle InvalidResponse errors."""
    return '', 401


@app.errorhandler(MimeError)
async def invalid_mime_err(_err):
    """Handle MimeError errors."""
    return '', 400


@app.route('/', methods=['GET'])
async def index():
    """Main index handler"""
    return 'hewwo'
