# mediaproxy: mediaproxy component of litecord
# Copyright 2018-2019, Luna Mendes and the mediaproxy contributors
# SPDX-License-Identifier: AGPL-3.0-only

import asyncio
import time

from quart import Blueprint, current_app as app
from quart.ctx import copy_current_app_context

from logbook import Logger

bp = Blueprint('url_cache', __name__)
log = Logger(__name__)


CACHE_INVALIDATE = 30


async def ucache_get_or_req(url: str) -> str:
    """Get or request text from the URL cache."""
    now = time.monotonic()

    try:
        invalid_ts, text = app.url_cache[url]

        if now > invalid_ts:
            app.url_cache.pop(url)
            return await ucache_get_or_req(url)

        return text
    except KeyError:
        async with app.session.get(url) as resp:
            # TODO: assert resp.content-type text/html
            # TODO: assert resp.status is 200
            text = await resp.text()

        app.url_cache[url] = (
            now + CACHE_INVALIDATE, text)
        return text


async def cache_janitor_tick(app_):
    """url cache janitor tick."""
    now = time.monotonic()

    for key in list(app_.url_cache):
        invalid_ts, _ = app_.url_cache[key]

        if invalid_ts > now:
            continue

        log.debug('removed {!r} from url cache', key)
        app_.url_cache.pop(key)



@bp.before_app_request
async def url_cache_start():
    """Start the url cache."""
    log.info('url cache start')
    app.url_cache = {}

    loop = asyncio.get_event_loop()

    @copy_current_app_context
    async def cache_janitor():
        """cache janitor loop"""
        try:
            log.info('url cache janitor start')
            while True:
                log.debug('url cache janitor tick')
                await cache_janitor_tick(app)
                await asyncio.sleep(30)
        except asyncio.CancelledError:
            log.info('url cache janitor cancel')
        except Exception:
            log.exception('url cache janitor fail')

    if not app.testing:
        loop.create_task(cache_janitor())
